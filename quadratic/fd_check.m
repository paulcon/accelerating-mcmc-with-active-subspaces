function r = fd_check(d)
% finite difference check
xx = randn(1,2); del = 1e-6;
df1 = (modelz(xx(1)+del,xx(2)) - modelz(xx(1),xx(2)))/del;
df2 = (modelz(xx(1),xx(2)+del) - modelz(xx(1),xx(2)))/del;
[~,dfm] = modelz(xx(1),xx(2));
if norm(df1-dfm(1))>1e-4 || norm(df2-dfm(2))>1e-4
    r1 = 0;
else
    r1 = 1;
end

df1 = (misfitz(xx(1)+del,xx(2),d) - misfitz(xx(1),xx(2),d))/del;
df2 = (misfitz(xx(1),xx(2)+del,d) - misfitz(xx(1),xx(2),d))/del;
[~,dfm] = misfitz(xx(1),xx(2),d);
if norm(df1-dfm(1))/norm(dfm(1))>1e-4 || norm(df2-dfm(2))/norm(dfm(2))>1e-4
    r2 = 0;
else
    r2 = 1;
end

r = r1*r2;

end