%%
% Generate 'small_mc.mat' containing the MCMC runs used in fig52_sec51.m
% Quadratic example from Section 5.1
%
% Needs PMPack
% http://www.mathworks.com/matlabcentral/fileexchange/29228-pmpack-parameterized-matrix-package
close all; clear all;

epsilon = 0.01; d = 0.9;
propsig2 = 0.5;
N = 1e6;
rng(44);
%%
% Metropolis-Hatings

m = 2;
X = zeros(N,m);
x0 = randn(1,m);
m0 = misfitz(x0(1),x0(2),epsilon,d);
accept = zeros(N,1);
for i=1:N
    
    X(i,:) = x0;
    delta = sqrt(propsig2)*randn(1,m);
    xc = x0 + delta;
    mc = misfitz(xc(1),xc(2),epsilon,d);
    
    gamma = min(1,exp(m0 + 0.5*(x0*x0') - mc - 0.5*(xc*xc')));
    t = rand(1,1);
    if gamma >= t
        x0 = xc; m0 = mc;
        accept(i) = 1;
    end
    
end


%%
% Active subspace-accelerated MCMC
s = [parameter('hermite'); parameter('hermite')];
[p,w] = gaussian_quadrature(s,[50 50]);
[~,G] = misfitz(p(:,1),p(:,2),epsilon,d);
C = G'*(bsxfun(@times,G,w));
[W,Sig] = eig(C);
[lam,ind] = sort(diag(Sig),'descend');
W = W(:,ind); W1 = W(:,1); W2 = W(:,2);

n = 1;
N_as = 100000;
Y = zeros(N_as,n);
x0 = randn(1,m);
y0 = W1'*x0';
Nz = 10;
[z,wz] = gaussian_quadrature(parameter('hermite'),Nz);

% evaluate AS approx of likelihood
XX = (W1*(y0*ones(1,Nz)) + W2*z')';
mm = misfitz(XX(:,1),XX(:,2),epsilon,d);
m0 = mm'*wz;

accept_as = zeros(N_as,1);
for i=1:N_as
    
    Y(i) = y0;
    delta = sqrt(propsig2)*randn(1,n);
    yc = y0 + delta;
    
    % evaluate AS approx of likelihood
    XX = (W1*(yc*ones(1,Nz)) + W2*z')';
    mm = misfitz(XX(:,1),XX(:,2),epsilon,d);
    mc = mm'*wz;
    
    gamma = min(1,exp(m0 + 0.5*(y0*y0') - mc - 0.5*(yc*yc')));
    t = rand(1,1);
    if gamma >= t
        y0 = yc; m0 = mc;
        accept_as(i) = 1;
    end
    
end

fprintf('Accept: %6.4f\n', sum(accept)/numel(accept));
fprintf('Accept: %6.4f\n', sum(accept_as)/numel(accept_as));


%%
% Reconstruct the x chain from the y chain.
X_as = zeros(N,2);
NNz = 10;
for i=1:N/10
    X_as((i-1)*10+1:i*10,:) = (W1*(Y(i)*ones(1,NNz)) + W2*randn(1,NNz))';
end

%%
save('small_mc.mat','X','X_as','Y','accept','accept_as');




