%%
% Figure 5.1 from Section 5.1
%
% Needs PMPack
% http://www.mathworks.com/matlabcentral/fileexchange/29228-pmpack-parameterized-matrix-package

close all; clear all;

%%
N = 101;
xx = linspace(-4,4,N);
[X1,X2] = meshgrid(xx,xx);

% Set epsilon in (5.2). 
% Figures 5.1a, b, and c use epsilon=0.01
% Figures 5.1d, e, and f use epsilon=0.95

epsilon = 0.01;
d = 0.9;

Z = reshape(misfitz(X1(:),X2(:),epsilon,d),N,N);
lik = exp(-Z);
tau2 = 1; 
pr = (2*pi)^(-0.5)*(tau2^(-1))*(exp(-(X1.^2+X2.^2)/(2*tau2)));

% get active subspace of misfit
s = [parameter('hermite'); parameter('hermite')];
[p,w] = gaussian_quadrature(s,[50 50]);
[~,G] = misfitz(p(:,1),p(:,2),epsilon,d);
C = G'*(bsxfun(@times,G,w));
[W,Sig] = eig(C);
[lam,ind] = sort(diag(Sig),'descend');
W = W(:,ind); W1 = W(:,1); W2 = W(:,2);

% Figs 5.1a, d
figure(1);
semilogy(1:2,lam,'ko','MarkerSize',18,'LineWidth',6);
axis square; grid on;
set(gca,'FontSize',18);
xlim([0 3]); ylim([1e-1 5e4]);
xlabel('Index'); ylabel('Eigenvalues');
print(sprintf('figs/evals_d%0.4d',int32(epsilon*100)),'-depsc2','-r300');

% Figs 5.1b, e
figure(2);
plot(1:2,W1,'kx','MarkerSize',18,'LineWidth',6);
axis square; grid on;
set(gca,'FontSize',18);
xlim([0 3]); ylim([-1 1]);
xlabel('Index'); ylabel('First eigenvector components');
print(sprintf('figs/evec1_d%0.4d',int32(epsilon*100)),'-depsc2','-r300');

% compute the constant with Gauss quadrature
pt0 = exp(-misfitz(p(:,1),p(:,2),epsilon,d));
pt1 = (2*pi)^(-0.5)*(tau2^(-1))*(exp(-(p(:,1).^2+p(:,2).^2)/(2*tau2)));
post = pt0.*pt1;
cpost = post'*w;

% Fig 5.1c, f
figure(3);
contour(X1,X2,(lik.*pr)/cpost,20);
grid on;
axis square;
xlabel('$x_1$','Interpreter','LaTeX'); 
ylabel('$x_2$','Interpreter','LaTeX'); 
set(gca,'FontSize',17);
print(sprintf('figs/post_d%0.4d',int32(epsilon*100)),'-depsc2','-r300');
    




