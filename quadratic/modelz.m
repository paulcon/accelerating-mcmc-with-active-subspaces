function [m, dm] = modelz(x1,x2,epsilon)
% Forward model and its derivative for Section 5.1
Q = 0.5*[sqrt(2) sqrt(2); -sqrt(2) sqrt(2)];
Aeig = diag([1 epsilon]);
A = Q*(Aeig*Q');
X = [x1 x2];
m = 0.5*sum((X*A).*X,2);
dm = X*A;

end