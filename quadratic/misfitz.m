function [misfit, dmisfit] = misfitz(x1,x2,epsilon,d)
% Misfit and gradient of misfit for Section 5.1
sig2 = 0.1^2;
[m, dm] = modelz(x1,x2,epsilon);
misfit = (m - d).^2./(2*sig2);
dmisfit = bsxfun(@times,dm,(m - d))/(sig2);

end


