%%
% Figure 5.2 from Section 5.1. 
close all; clear all;

% Requires kde2d from File Exchange:
% http://www.mathworks.com/matlabcentral/fileexchange/17204-kde2d-data-n-min-xy-max-xy-
load('small_mc.mat');
[band,MCKDE,XX1,XX2] = kde2d(X,2^7,[-4,-4],[4,4]);
[band_as,MCKDE_as,XX1_as,XX2_as] = kde2d(X_as,2^7,[-4,-4],[4,4]);
[ff,xxi] = ksdensity(Y);

fontsize = 17;
%%
% Fig 5.2a
figure(1); 
contour(XX1,XX2,MCKDE,20); grid on;
axis square; 
set(gca,'FontSize',fontsize);
xlabel('$x_1$','Interpreter','LaTeX'); 
ylabel('$x_2$','Interpreter','LaTeX'); 
print(sprintf('figs/mc_postx'),'-depsc2','-r300');

% Fig 5.2b
figure(2); 
contour(XX1_as,XX2_as,MCKDE_as,20); grid on;
axis square;
set(gca,'FontSize',fontsize);
xlabel('$x_1$','Interpreter','LaTeX'); 
ylabel('$x_2$','Interpreter','LaTeX'); 
print(sprintf('figs/mc_as_postx'),'-depsc2','-r300');

% Fig 5.2c
figure(3);
plot(xxi,ff,'k-','LineWidth',2);
axis square; grid on;
set(gca,'FontSize',fontsize);
xlabel('$y$','Interpreter','LaTeX'); 
ylabel('$\hat{\pi}_{\varepsilon}(y)$','Interpreter','LaTeX'); 
print(sprintf('figs/mc_as_posty'),'-depsc2','-r300');

% Fig 5.2d
indz = 600001:605000;
figure(4);
plot(indz,X(indz,:),'-','LineWidth',1.5);
axis square; grid on;
set(gca,'FontSize',fontsize);
xlabel('Iteration');
ylabel('Markov chain states');
legend('x_1','x_2','Location','NorthEast');
print(sprintf('figs/trace_x'),'-depsc2','-r300');

% Fig 5.2e
figure(5);
plot(indz,X_as(indz,:),'-','LineWidth',1.5);
axis square; grid on;
set(gca,'FontSize',fontsize);
xlabel('Iteration');
ylabel('Markov chain states');
legend('x_1','x_2','Location','NorthEast');
print(sprintf('figs/trace_x_as'),'-depsc2','-r300');

% Fig 5.2f
figure(6);
indz_y = 60001:70000;
plot(indz_y,Y(indz_y),'-','LineWidth',1.5);
axis square; grid on;
set(gca,'FontSize',fontsize);
xlabel('Iteration');
ylabel('Markov chain states');
legend('y','Location','NorthEast');
print(sprintf('figs/trace_y'),'-depsc2','-r300');





