function [sig2,halfwidth] = monte_carlo_standard_error(X,theta)
if ~exist('theta','var')
    theta = 0.5;
elseif theta >= 1
    error('theta >= 1');
end

[M,m] = size(X);

b = floor(M^theta);
a = floor(M/b);
g = mean(X,1);
Y = zeros(a,m);
for j=1:a
    Y(j,:) = mean(X((j-1)*b+1:j*b,:),1);
end

sig2 = (b/(a-1))*sum(bsxfun(@minus,Y,g).^2,1);
halfwidth = tinv(0.995,a-1)*(sqrt(sig2)/sqrt(M));



