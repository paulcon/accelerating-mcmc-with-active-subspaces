function mess = min_eff_sample_size(X,maxlag)

m = size(X,2);
mess = 1e9;
for i=1:m
    [ess,~] = effective_sample_size(X(:,i),maxlag);
    if ess < mess
        mess = ess;
    end
end
