%%
% Figures from Section 5.2.
% For the revision.
clear all; close all;
rng(42);
linewidth = 2;
fontsize = 16;
markersize = 14;
load('misfits.mat');
[~,Sig,W] = svd(dmisfits,'econ');

%%
% Figure 5.7
M_mc = [1 5 10 20 50 100 500];

load('d02_00.mat');
covar = bsxfun(@rdivide,(sqrt(MFv(:,2:end))./MFm(:,2:end)),sqrt(M_mc(2:end)));

figure(1);
semilogx(M_mc(2:end),mean(covar,1),'bo-',...
    'linewidth',linewidth);
axis square; grid on;
set(gca,'fontsize',fontsize);
xlabel('Number of samples');
ylabel('Average coefficient of variation');
ylim([0 0.06]);
pname = sprintf('figs/mc_covz_d0%d',i);
print(pname,'-depsc2');
    
%%
% Compute autocorrelations and reconstruct X chain from Y samples
load('vsig10_0.mat');
Xvan = X_chain;
maxlagz = 50; steps = 1;
[lvan,acfvan] = autocorrelation(Xvan(:,10),maxlagz,steps);

fnames = {'d2sig10_0.mat','d2sig30_0.mat'};
Xas2 = cell(2,1);
Yas2 = cell(2,1);
for k=1:2
    load(fnames{k});
    m = 100; n = 2;
    Yas2{k} = X_chain*W(:,1:n);
    XX = zeros(size(Xvan));
    stepsize = 10;
    for i=1:size(Yas2{k},1)
        Z = randn(m-n,stepsize);
        XX((i-1)*stepsize+1:i*stepsize,:) = ...
            (bsxfun(@plus,W(:,1:n)*Yas2{k}(i,:)',W(:,n+1:end)*Z))';
    end
    [las2{k},acfas2{k}] = autocorrelation(XX(:,10),maxlagz,steps);
    Xas2{k} = XX;
end

% Compute effective sample size
tic;
messvan = min_eff_sample_size(Xvan,2000);
fprintf('Vanilla min ESS: %10.8f, time: %4.2f\n',messvan,toc);
tic;
messas2x_s1 = min_eff_sample_size(Xas2{1},2000);
fprintf('AS2x,s1 min ESS: %10.8f, time: %4.2f\n',messas2x_s1,toc);
tic;
messas2x_s3 = min_eff_sample_size(Xas2{2},2000);
fprintf('AS2x,s3 min ESS: %10.8f, time: %4.2f\n',messas2x_s3,toc);
tic;
messas2y_s1 = min_eff_sample_size(Yas2{1},2000);
fprintf('AS2y,s1 min ESS: %10.8f, time: %4.2f\n',messas2y_s1,toc);
tic;
messas2y_s3 = min_eff_sample_size(Yas2{2},2000);
fprintf('AS2y,s3 min ESS: %10.8f, time: %4.2f\n',messas2y_s3,toc);


%%
% Fig 5.8a
figure(1);
plot(lvan,acfvan,'ko-',...
    las2{1},acfas2{1},'ro-',...
    las2{2},acfas2{2},'bo-',...
    'linewidth',linewidth,'markersize',8);
axis square; grid on;
set(gca,'FontSize',fontsize);
xlabel('Lag'); ylabel('Autocorrelation');
legend('Vanilla','AS2, 0.1','AS2, 0.3','location','east');
xlim([0 maxlagz+1]);
pname = sprintf('figs/acf');
print(pname,'-depsc2');

% Fig 5.8b
indz = 100000:102000;
figure(2);
plot(indz,Xvan(indz,10),'k-','linewidth',linewidth);
axis square; grid on;
set(gca,'FontSize',fontsize);
xlabel('Step'); ylabel('State');
xlim([indz(1) indz(end)]);
pname = sprintf('figs/van_x');
print(pname,'-depsc2');

% Fig 5.8c
figure(3);
plot(indz,Xas2{1}(indz,10),'k-','linewidth',linewidth);
axis square; grid on;
set(gca,'FontSize',fontsize);
xlabel('Step'); ylabel('State');
xlim([indz(1) indz(end)]);
pname = sprintf('figs/as2s1_x');
print(pname,'-depsc2');

% Fig 5.8d
figure(4);
plot(indz,Xas2{2}(indz,10),'k-','linewidth',linewidth);
axis square; grid on;
set(gca,'FontSize',fontsize);
xlabel('Step'); ylabel('State');
xlim([indz(1) indz(end)]);
pname = sprintf('figs/as2s3_x');
print(pname,'-depsc2');

%%
% Fig 5.10
% bivariate posterior marginals
indz = [2 3 4 3 4 4; 1 1 1 2 2 3];
bw = 0.001;
for k=1:6
    a = indz(1,k); b = indz(2,k);
    Y = Xvan(10000:end,:)*(-W(:,[a b]));
    [band,MCKDE,XX1,XX2] = kde2d(Y,2^6,[-3,-3],[3,3],bw);
    figure(k); 
    contour(XX1,XX2,MCKDE,15,'LineWidth',3); grid on;
    axis square; 
    %set(gca,'FontSize',fontsize);
    set(gca,'FontSize',fontsize,'LineWidth',3);
    xlabel(sprintf('$y_%d$',a),'Interpreter','LaTeX'); 
    ylabel(sprintf('$y_%d$',b),'Interpreter','LaTeX'); 
    print(sprintf('figs/van%d%d',a-1,b-1),'-depsc2');
end

%%
% Checking near-Gaussianity of posterior marginals. Figures not used in the
% paper.
% bw = 0.001;
% for k=1:99
%     Y = Xvan(10000:end,:)*(-W(:,[k k+1]));
%     [band,MCKDE,XX1,XX2] = kde2d(Y,2^6,[-3,-3],[3,3],bw);
%     figure(1); 
%     contour(XX1,XX2,MCKDE,15); grid on;
%     axis square; 
%     set(gca,'FontSize',fontsize);
%     xlabel(sprintf('$y_%d$',k),'Interpreter','LaTeX'); 
%     ylabel(sprintf('$y_%d$',k+1),'Interpreter','LaTeX'); 
%     pause;
% end



%%
% Posterior mean and variance
mvan = mean(Xvan,1); theta = 2/3;
[~,hw_mvan] = monte_carlo_standard_error(Xvan,theta);
Xvan_cen = bsxfun(@minus,Xvan,mvan).^2;
vvan = mean(Xvan_cen);
[~,hw_vvan] = monte_carlo_standard_error(Xvan_cen);

mas2s1 = mean(Xas2{1},1);
[~,hw_mas2s1] = monte_carlo_standard_error(Xas2{1});
Xas2s1_cen = bsxfun(@minus,Xas2{1},mas2s1).^2;
vas2s1 = mean(Xas2s1_cen);
[~,hw_vas2s1] = monte_carlo_standard_error(Xas2s1_cen);

mas2s3 = mean(Xas2{2},1);
[~,hw_mas2s3] = monte_carlo_standard_error(Xas2{2});
Xas2s3_cen = bsxfun(@minus,Xas2{2},mas2s3).^2;
vas2s3 = mean(Xas2s3_cen);
[~,hw_vas2s3] = monte_carlo_standard_error(Xas2s3_cen);

%%
% Fig 5.9a
close all;
figure(3);
hl = plot(1:m,mas2s1-mvan,'ko',...
    'MarkerSize',6,'LineWidth',linewidth,'MarkerFaceColor','k');
axis square; grid on; 
set(gca,'FontSize',fontsize);
ylim([-0.2 0.2]);
xlabel('Index'); ylabel('Centered posterior mean');
hold on;
hp = patch([(1:m)';flipud((1:m)')],...
    [(hw_mvan)'; flipud((-hw_mvan)')],...
    [0.7 0.7 0.7],'EdgeColor',[0.7 0.7 0.7]);
uistack(hp,'bottom');
hold off;
pname = sprintf('figs/mas2s1');
print(pname,'-depsc2');

% Fig 5.9b
figure(4);
hl = plot(1:m,mas2s3-mvan,'ko',...
    'MarkerSize',6,'LineWidth',linewidth,'MarkerFaceColor','k');
axis square; grid on; 
set(gca,'FontSize',fontsize);
ylim([-0.2 0.2]);
xlabel('Index'); ylabel('Centered posterior mean');
hold on;
hp = patch([(1:m)';flipud((1:m)')],...
    [(hw_mvan)'; flipud((-hw_mvan)')],...
    [0.7 0.7 0.7],'EdgeColor',[0.7 0.7 0.7]);
uistack(hp,'bottom');
hold off;
pname = sprintf('figs/mas2s3');
print(pname,'-depsc2');

% Fig 5.9c
figure(5);
hl = plot(1:m,vas2s1-vvan,'ko',...
    'MarkerSize',6,'LineWidth',linewidth,'MarkerFaceColor','k');
axis square; grid on; 
set(gca,'FontSize',fontsize);
ylim([-0.2 0.2]);
xlabel('Index'); ylabel('Centered posterior variance');
hold on;
hp = patch([(1:m)';flipud((1:m)')],...
    [(hw_vvan)'; flipud((-hw_vvan)')],...
    [0.7 0.7 0.7],'EdgeColor',[0.7 0.7 0.7]);
uistack(hp,'bottom');
hold off;
pname = sprintf('figs/vas2s1');
print(pname,'-depsc2');

% Fig 5.9d
figure(6);
hl = plot(1:m,vas2s3-vvan,'ko',...
    'MarkerSize',6,'LineWidth',linewidth,'MarkerFaceColor','k');
axis square; grid on; 
set(gca,'FontSize',fontsize);
ylim([-0.2 0.2]);
xlabel('Index'); ylabel('Centered posterior variance');
hold on;
hp = patch([(1:m)';flipud((1:m)')],...
    [(hw_vvan)'; flipud((-hw_vvan)')],...
    [0.7 0.7 0.7],'EdgeColor',[0.7 0.7 0.7]);
uistack(hp,'bottom');
hold off;
pname = sprintf('figs/vas2s3'); 
print(pname,'-depsc2');






