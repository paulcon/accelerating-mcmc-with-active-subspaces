function [ess,acf] = effective_sample_size(X,maxlag)

M = length(X);
[~,acf] = autocorrelation(X,maxlag,1);
ess = M/(1+2*sum(acf));
