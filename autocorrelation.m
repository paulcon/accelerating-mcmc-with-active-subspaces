function [lagz,acf] = autocorrelation(X,maxlag,step)

m = mean(X); v = var(X);
lagz = 1:step:maxlag+1;
N = length(lagz);
acf = zeros(N,1);
for i=1:N
    lag = lagz(i);
    X1 = X(1:end-lag);
    X2 = X(lag+1:end);
    acf(i) = sum((X1-m).*(X2-m))/(v*length(X2));
end